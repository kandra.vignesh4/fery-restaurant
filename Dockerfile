# pull official base image

FROM node:current

# set working directory
 
WORKDIR /fery-restaurant
# add `/node_modules/.bin` to $PATH

ENV PATH /node_modules/.bin:$PATH

# install app dependencies

COPY package.json ./


RUN npm i -g npm-check-updates
RUN ncu -u


RUN npm install

RUN npm install -g @angular/cli@latest



RUN npm install --save core-js@^2.5.0

RUN ng build --prod

COPY . .

CMD ["npm","start"]

